package com.example.hp.miniapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToTab2(View v)
    {
        Intent tab2 = new Intent(this, MainActivity2.class);
        startActivity(tab2);
    }

    public void goToTab3(View v)
    {
        Intent tab3 = new Intent(this, MainActivity3.class);
        startActivity(tab3);
    }

    public void goToTab4(View v)
    {
        Intent tab4 = new Intent(this, MainActivity4.class);
        startActivity(tab4);
    }
}
